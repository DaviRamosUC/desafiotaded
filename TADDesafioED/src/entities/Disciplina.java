package entities;

import java.util.List;

import services.DisciplinaService;

public class Disciplina implements DisciplinaService {

	private String nome;
	private int semestre;
	
	public Disciplina() {
		
	}
	
	public Disciplina(String nome, int semestre) {
		this.nome = nome;
		this.semestre = semestre;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	@Override
	public boolean consultar(List<Disciplina> disciplina, Disciplina disciplinaa) {
		boolean t =disciplina.contains(disciplinaa);
		return t;
	}

	@Override
	public void incluir(List<Disciplina> disciplina, Disciplina disciplinaa) {
		disciplina.add(disciplinaa);
		
	}

	@Override
	public void remover(List<Disciplina> disciplina, Disciplina disciplinaa) {
		disciplina.remove(disciplinaa);
		
	}

	@Override
	public int disciplinaSize(List<Disciplina> disciplina) {
		return disciplina.size();
		
	}

	@Override
	public void disciplinaClear(List<Disciplina> disciplina) {
		disciplina.clear();
	}

	
}
