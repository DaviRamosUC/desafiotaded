package services;

import java.util.List;

import entities.Disciplina;

public interface DisciplinaService {

	public boolean consultar (List <Disciplina> disciplina, Disciplina disciplinaa);
	
	public void incluir (List <Disciplina> disciplina, Disciplina disciplinaa);
	
	public void remover (List <Disciplina> disciplina, Disciplina disciplinaa);
	
	public int disciplinaSize (List <Disciplina> disciplina);

	public void disciplinaClear (List <Disciplina> disciplina);
}
