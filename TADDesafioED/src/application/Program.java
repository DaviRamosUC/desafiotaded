package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Disciplina;

public class Program {
	
	public static void main (String[]args) {
		
		Scanner sc = new Scanner(System.in);
		List<Disciplina> disciplina = new ArrayList<>();
		
		String nome;
		int semestre;
		Disciplina dsc = new Disciplina();
		
		char saida='s';
		while (saida=='s') {
			System.out.println("Informe o nome da disciplina");
			nome = sc.nextLine();
			System.out.println("Informe o semestre da disciplina");
			semestre = sc.nextInt();
			

			dsc = new Disciplina(nome, semestre);
			
			dsc.incluir(disciplina, dsc);
			sc.nextLine();
			System.out.println("Deseja cadastrar outra disciplina? s/n");
			saida=sc.nextLine().charAt(0);
		}
		
		String resposta = (dsc.consultar(disciplina, dsc)) ? "Existe" : "N�o existe";
		
		System.out.println("A disciplina: "+resposta);
		
		dsc.remover(disciplina, dsc);

		dsc.incluir(disciplina, dsc);
		
		System.out.println("O quantidade de professores �: "+dsc.disciplinaSize(disciplina));
		System.out.println("\nLimpando todas as Disciplinas");
		dsc.disciplinaClear(disciplina);

		System.out.println("O quantidade de professores �: "+dsc.disciplinaSize(disciplina));
		
		sc.close();
		
	}
}
